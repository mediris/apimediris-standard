<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestedProcedure;
use App\ServiceRequest;
use App\Http\Requests;
use App\Addendums;
use Activity;
use Log;
use DB;

class AddendumsController extends Controller
{
    public function add(Request $request)
    {

        if($request->isMethod('POST')) {

            $this->validate($request, [
                'user_id' => 'required',
                'text' => 'required',
                'requested_procedure_id' => 'required'
            ]);

            try {

                $addendum = new Addendums($request->all());
                $addendum->active = 1;
                $addendum->date = date('Y-m-d G:i:s');
                
                $addendum->save();

                Activity::log(trans('tracking.add', ['section' => 'addendum/add', 'id' => $addendum->id]));


            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/radiologist/radiologist.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: radiologist/addendum. Action: approve');
                $errorMessage = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                return response()->json(['error' => $e->getCode(), 'message' => $errorMessage]);
            }

            $addendum->load('requestedProcedure');
            /*$requestedProcedure = RequestedProcedure::find( $addendum->requested_procedure_id );
            $requestedProcedure->load(['serviceRequest', 'procedure', 'requestedProcedureStatus', 'platesSize']);
            $requestedProcedure->load(['addendums' => function ($query) use ($requestedProcedure){
                                    $query->where('requested_procedure_id', $requestedProcedure->id)
                                            ->orderBy('id', 'desc')
                                            ->limit(1);
            }]);
            $requestedProcedure->serviceRequest->load(['requestDocuments', 'source', 'patientType', 'requestStatus', 'smokingStatus', 'answer', 'pregnancyStatus', 'referring']);*/
            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $addendum->id, 'newValue' => $addendum]);
        }
    }

}
