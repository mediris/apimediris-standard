<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\Http\Requests;
use Activity;
use Log;

class UnitsController extends Controller
{
    public function index()
    {
        try
        {
            $units = Unit::orderBy('description', 'asc')->get();
            return $units;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: units. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    public function show(Unit $unit, Request $request)
    {

        /**
         * Log activity
         */
        Activity::log(trans('tracking.show', ['section' => 'units', 'id' => $unit->id]), $request->all()['user_id']);

        return $unit;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
            ]);

            $unit = new Unit($request->all());

            try
            {
                if($unit->save())
                {
                    /**
                     * Log activity
                     */
                    Activity::log(trans('tracking.create', ['section' => 'units', 'id' => $unit->id]), $request->all()['user_id']);
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'units', 'action' => 'create']), $request->all()['user_id']);
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: units. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $unit->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, Unit $unit)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:25',
            ]);

            $original = new Unit();
            foreach($unit->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $unit->active = 0;

            try
            {
                if($unit->update($request->all()))
                {
                    /**
                     * Log activity
                     */
                    Activity::log(trans('tracking.edit', ['section' => 'units', 'id' => $unit->id, 'oldValue' => $original, 'newValue' => $unit]), $request->all()['user_id']);
                }
                else
                {
                    /**
                     * Log activity
                     */
                    Activity::log(trans('tracking.attempt-edit', ['id' => $unit->id, 'section' => 'units', 'action' => 'edit']), $request->all()['user_id']);
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: units. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $unit]);
        }
        return $unit;
    }

    public function delete(Request $request, Unit $unit)
    {
        try
        {
            if($unit->delete())
            {
                /**
                 * Log activity
                 */
                Activity::log(trans('tracking.delete', ['id' => $unit->id, 'section' => 'units']), $request->all()['user_id']);
            }
            else
            {
                /**
                 * Log activity
                 */
                Activity::log(trans('tracking.attempt-edit', ['id' => $unit->id, 'section' => 'units', 'action' => 'delete']), $request->all()['user_id']);
            }

            return response()->json(['code' => '200', 'message' => 'Deleted']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: units. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function active(Request $request, Unit $unit)
    {
        try
        {
            $original = new Unit();

            foreach($unit->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }
            $unit->active();

            /**
             * Log activity
             */
            Activity::log(trans('tracking.edit', ['section' => 'units', 'id' => $unit->id, 'oldValue' => $original, 'newValue' => $unit, 'action' => 'active']), $request->all()['user_id']);
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: units. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $unit]);
    }
}
