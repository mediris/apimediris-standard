<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consumable;
//use App\Institution;
use App\Http\Requests;
use Activity;
use Log;

class ConsumablesController extends Controller
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una colección en formato Json de Consumables.
     */
    public function index()
    {
        try {
            $consumables = Consumable::all();
            $consumables->load('unit');
            return $consumables;
        } catch (\Exception $e) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }

    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Retornar una instancia de Consumable.
     */
    public function show(Consumable $consumable, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'consumable', 'id' => $consumable->id]), $request->all()['user_id']);

        $consumable->load('unit');
        return $consumable;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Agregar una instancia de Consumable.
     */
    public function add(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'description' => 'required|max:25',
                'unit_id' => 'required',
            ]);

            $consumable = new Consumable($request->all());

            try {
                if ($consumable->save()) {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.create', ['section' => 'consumable', 'id' => $consumable->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.consumable')]));
                    $request->session()->flash('class', 'alert alert-success');
                } else {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt', ['section' => 'consumable', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.consumable')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Created', 'id' => $consumable->id]);
        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    /**
     * @param Request $request
     * @param Consumable $consumable
     * @return Consumable|\Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, Consumable $consumable)
    {

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'description' => 'required|max:25',
                'unit_id' => 'required',
            ]);

            $original = new Consumable();
            foreach ($consumable->getOriginal() as $key => $value) {
                $original->$key = $value;
            }

            $consumable->active = 0;

            try {
                if ($consumable->update($request->all())) {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'consumable', 'id' => $consumable->id, 'oldValue' => $original, 'newValue' => $consumable]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.consumable')]));
                    $request->session()->flash('class', 'alert alert-success');
                } else {

                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $consumable->id, 'section' => 'consumable', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.consumable')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            } catch (\Exception $e) {
                Log::useFiles(storage_path() . '/logs/admin/admin.log');
                Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $consumable]);
        }
        return $consumable;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Borrar una instancia de Consumable (NO ESTÁ EN USO).
     */
    public function delete(Request $request, Consumable $consumable)
    {
        try {
            if ($consumable->delete()) {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.delete', ['id' => $consumable->id, 'section' => 'consumable']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.consumable')]));
                $request->session()->flash('class', 'alert alert-success');
            } else {

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.attempt-edit', ['id' => $consumable->id, 'section' => 'consumable', 'action' => 'delete']), $request->all()['user_id']);

                $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.consumable')]));
                $request->session()->flash('class', 'alert alert-danger');
            }

            return response()->json(['code' => '200', 'message' => 'Deleted']);
        } catch (\Exception $e) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: delete');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el estado de activo o no de un Consumable.
     */
    public function active(Request $request, Consumable $consumable)
    {
        try {

            $original = new Consumable();
            foreach ($consumable->getOriginal() as $key => $value) {
                $original->$key = $value;
            }

            $consumable->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'consumables', 'id' => $consumable->id, 'oldValue' => $original, 'newValue' => $consumable, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        } catch (\Exception $e) {
            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: consumables. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $consumable]);
    }
}
