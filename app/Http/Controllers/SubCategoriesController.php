<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubCategory;
use App\Http\Requests;
use Activity;
use Log;

class SubCategoriesController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $subcategories = SubCategory::where($where)->get();
            } else {
                $subcategories = SubCategory::all();
            }

            return $subcategories;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: subcategories. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->getMessage()]);
        }
    }

    public function show(SubCategory $subcategory, Request $request)
    {

        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'subcategories', 'id' => $subcategory->id]), $request->all()['user_id']);


        return $subcategory;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:45',
                'language' => 'required|max:10',
            ]);

            $subcategory = new SubCategory($request->all());

            try
            {
                $subcategory->save();

                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.create', ['section' => 'subcategories', 'id' => $subcategory->id]), $request->all()['user_id']);

                return response()->json(['code' => '201', 'message' => 'Created', 'id' => $subcategory->id]);

            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: subcategories. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

        }
        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, SubCategory $subcategory)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:45',
                'language' => 'required|max:10',
            ]);

            $original = new SubCategory();
            foreach($subcategory->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $subcategory->active = 0;

            try
            {
                $subcategory->update($request->all());


                /**
                 * Log activity
                 */

                Activity::log(trans('tracking.edit', ['section' => 'subcategories', 'id' => $subcategory->id, 'oldValue' => $original, 'newValue' => $subcategory]), $request->all()['user_id']);

                return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $subcategory]);


            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: subcategories. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

        }
        return $subcategory;
    }

    public function active(Request $request, SubCategory $subcategory)
    {
        try
        {

            $original = new SubCategory();
            foreach($subcategory->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            $subcategory->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'subcategories', 'id' => $subcategory->id, 'oldValue' => $original, 'newValue' => $subcategory, 'action' => 'active']), $request->all()['user_id']);

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $subcategory]);

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: subcategories. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }

    }
}
