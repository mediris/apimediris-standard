<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuspendReason;
use App\Http\Requests;
use Activity;
use Log;

class SuspendReasonsController extends Controller
{
    /**
     * @fecha: 23/06/2017
     * @parametros:
     * @programador: Ricmary Bron
     * @objetivo: Controlador para el manejo de los motivos de suspension
     */

    public function index(Request $request)
    {
        try
        {
            $routeTokens = $request->route()->getCompiled()->getTokens();
            if ( strpos( $routeTokens[0][1], "showactive" ) !== false ) {
                $suspendreasons = SuspendReason::where('active', true)->orderBy('description', 'asc')->get();
            } else {

                if ( isset($request->all()['where']) ) {
                    $where = $request->all()['where'];
                    $suspendreasons = SuspendReason::where($where)->orderBy('description', 'asc')->get();
                } else {
                    $suspendreasons = SuspendReason::orderBy('description', 'asc')->get();
                }
            }

            return $suspendreasons;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Suspend reasons. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(SuspendReason $suspend_reason)
    {
        return $suspend_reason;
    }

    public function add(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:150'
            ]);

            $suspend_reason = new SuspendReason($request->all());

            try
            {
                if( $suspend_reason->save()) {
                    Activity::log(trans('tracking.create', ['section' => 'suspend_reasons', 'id' => $suspend_reason->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.suspendreason')]));
                    $request->session()->flash('class', 'alert alert-success');
                } else {
                    Activity::log(trans('tracking.attempt', ['section' => 'suspend_reasons', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.suspendreason')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Suspend reasons. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $suspend_reason->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit(Request $request, SuspendReason $suspend_reason)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'description' => 'required|max:70'
            ]);
            
            // $suspend_reason->active = 0;

            $original = new SuspendReason();
            foreach($suspend_reason->getOriginal() as $key => $value)
            {
                $original->$key = $value;
            }

            try
            {
                if($suspend_reason->update($request->all()))
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.edit', ['section' => 'suspend_reasons', 'id' => $suspend_reason->id, 'oldValue' => $original, 'newValue' => $suspend_reason]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.suspendreason')]));
                    $request->session()->flash('class', 'alert alert-success');
                }
                else
                {
                    /**
                     * Log activity
                     */

                    Activity::log(trans('tracking.attempt-edit', ['id' => $suspend_reason->id, 'section' => 'suspend_reasons', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.suspendreason')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }
            catch(\Exception $e)
            {
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Suspend reasons. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $suspend_reason]);
        }
    }

    public function active(Request $request, SuspendReason $suspend_reason) {
        try {
            $original = new SuspendReason();
            foreach($suspend_reason->getOriginal() as $key => $value ) {
                $original->$key = $value;
            }
            $suspend_reason->active();

            Activity::log(trans('tracking.edit', ['section' => 'suspend_reasons', 'id' => $suspend_reason->id, 'oldValue' => $original, 'newValue' => $suspend_reason, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: Suspend reasons. Action: active');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }

        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $suspend_reason]);
    }

}