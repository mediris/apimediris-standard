<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RequestedProcedureStatus;
use Activity;
use Log;

class RequestedProcedureStatusController extends Controller {

    public function index(Request $request) {

        try {

            $requestedProcedureStatuses = RequestedProcedureStatus::orderBy('id', 'asc')->get();


            return $requestedProcedureStatuses;

        } catch (\Exception $e) {

            Log::useFiles(storage_path() . '/logs/admin/admin.log');
            Log::alert('Error code: ' . $e->getCode() . ' Error message: ' . $e->getMessage() . ' Section: requestedProcedureStatuses. Action: index');


            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);

        }
    }
}
