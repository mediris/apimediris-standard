<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedicalVisitor;
use App\User;
use Activity;
use Log;

use App\Http\Requests;

class MedicalVisitorsController extends Controller{

    public function index(Request $request){
        try {
            if ( isset($request->all()['where']) ) {
                $where = $request->all()['where'];
                $medical_visitors = MedicalVisitor::where($where)->orderBy('last_name', 'asc')->get();
            } else {
                $medical_visitors = MedicalVisitor::orderBy('last_name', 'asc')->get();
            }

            return $medical_visitors;
        }catch(\Exception $e){
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: medicalVisitors. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }

    public function show(MedicalVisitor $medicalvisitor, Request $request){
        /**
         * Log activity
         */

        Activity::log(trans('tracking.show', ['section' => 'medicalVisitors', 'id' => $medicalvisitor->id]), $request->all()['user_id']);
        
        return $medicalvisitor;
    }

    public function add(Request $request){

        if($request->isMethod('post')){
            $this->validate($request, [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'administrative_ID' => 'required|unique:medical_visitors|max:45',
                'email' => 'required|email|max:255|unique:medical_visitors',
                'medical_visitor_institution' => 'required|max:250',
                'speciality' => 'required|max:250',
            ]);

            $medical_visitors = new MedicalVisitor($request->all());

            try{
                if($medical_visitors->save()){
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.create', ['section' => 'medicalVisitors', 'id' => $medical_visitors->id]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-add', ['name' => trans('messages.medical-visitor')]));
                    $request->session()->flash('class', 'alert alert-success');
                }else{
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.attempt', ['section' => 'medicalVisitors', 'action' => 'create']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-add', ['name' => trans('messages.medical-visitor')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }catch(\Exception $e){
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: medicalVisitors. Action: add');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '201', 'message' => 'Created', 'id' => $medical_visitors->id]);
        }

        return response()->json(['error' => '400', 'message' => 'Bad Request']);
    }

    public function edit( Request $request, MedicalVisitor $medicalvisitor ){

        if($request->isMethod('post')){
            $this->validate($request, [
                'first_name' => 'required|max:50',
                'last_name' => 'required|max:50',
                'administrative_ID' => 'required|unique:medical_visitors,administrative_ID,'.$medicalvisitor->id.'|max:45',
                'email' => 'required|email|max:255|unique:medical_visitors,email,'.$medicalvisitor->id,
                'medical_visitor_institution' => 'required|max:250',
                'speciality' => 'required|max:250',
            ]);

            $original = new MedicalVisitor();
            foreach($medicalvisitor->getOriginal() as $key => $value){
                $original->$key = $value;
            }

            $medicalvisitor->active = 0;

            try{
                if($medicalvisitor->update($request->all())){
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.edit', ['section' => 'medicalVisitors', 'id' => $medicalvisitor->id, 'oldValue' => $original, 'newValue' => $medicalvisitor]), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.success-edit', ['name' => trans('messages.medical-visitor')]));
                    $request->session()->flash('class', 'alert alert-success');
                }else{
                    /**
                     * Log activity
                     */

                    //the index user_id to log was changed to user because the table already have a user_id field
                    Activity::log(trans('tracking.attempt-edit', ['id' => $medicalvisitor->id, 'section' => 'medicalVisitors', 'action' => 'edit']), $request->all()['user_id']);

                    $request->session()->flash('message', trans('messages.error-edit', ['name' => trans('messages.medical-visitor')]));
                    $request->session()->flash('class', 'alert alert-danger');
                }
            }catch(\Exception $e){
                Log::useFiles(storage_path().'/logs/admin/admin.log');
                Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: medicalVisitors. Action: edit');

                return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
            }

            return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $medicalvisitor]);
        }
        $users = User::all();
        return response()->json(['users' => $users, 'referring' => $medicalvisitor]);
    }

    // public function delete(Request $request, Referring $referring){
    //     try{
    //         if($referring->delete()){
    //             /**
    //              * Log activity
    //              */

    //             Activity::log(trans('tracking.delete', ['id' => $referring->id, 'section' => 'referring']), $request->all()['user_id']);

    //             $request->session()->flash('message', trans('messages.success-delete', ['name' => trans('messages.referring')]));
    //             $request->session()->flash('class', 'alert alert-success');
    //         }else{
    //             /**
    //              * Log activity
    //              */

    //             Activity::log(trans('tracking.attempt-edit', ['id' => $referring->id, 'section' => 'referring', 'action' => 'delete']), $request->all()['user_id']);

    //             $request->session()->flash('message', trans('messages.error-delete', ['name' => trans('messages.referring')]));
    //             $request->session()->flash('class', 'alert alert-danger');
    //         }
    //         return response()->json(['code' => '200', 'message' => 'Deleted']);
    //     }catch(\Exception $e){
    //         Log::useFiles(storage_path().'/logs/admin/admin.log');
    //         Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: referrings. Action: delete');

    //         return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
    //     }
    // }

    public function active(Request $request, MedicalVisitor $medicalvisitor){
        try{
            $original = new MedicalVisitor();
            foreach($medicalvisitor->getOriginal() as $key => $value){
                $original->$key = $value;
            }
            $medicalvisitor->active();

            /**
             * Log activity
             */

            Activity::log(trans('tracking.edit', ['section' => 'medicalVisitors', 'id' => $medicalvisitor->id, 'oldValue' => $original, 'newValue' => $medicalvisitor, 'action' => 'active']), $request->all()['user_id']);

            $request->session()->flash('message', trans('alerts.success-edit'));
            $request->session()->flash('class', 'alert alert-success');

        }catch(\Exception $e){
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: medicalVisitors. Action: active');
            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
        return response()->json(['code' => '200', 'message' => 'Updated', 'oldValue' => $original, 'newValue' => $medicalvisitor]);
    }
}
