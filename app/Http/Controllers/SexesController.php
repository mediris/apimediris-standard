<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sex;
use App\Http\Requests;
use Activity;
use Log;

class SexesController extends Controller
{
    public function index(Request $request)
    {
        try
        {
            $sexes = Sex::all();
            return $sexes;
        }
        catch(\Exception $e)
        {
            Log::useFiles(storage_path().'/logs/admin/admin.log');
            Log::alert('Error code: '.$e->getCode().' Error message: '.$e->getMessage().' Section: sexes. Action: index');

            return response()->json(['error' => $e->getCode(), 'message' => $e->errorInfo[2]]);
        }
    }
}
