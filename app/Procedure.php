<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
        'description', 'active', 'interview', 'mammography', 'worklist', 'images', 'radiologist', 'radiologist_fee', 'technician', 'technician_fee', 'transcriptor_fee', 'administrative_ID', 'indications', 'modality_id'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Procedures tienen muchos Orders.
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Procedures tienen muchos Templates.
     */
    public function templates()
    {
        return $this->belongsToMany(Template::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Procedures tienen muchos Steps. Además contiene un campo llamado 'order'.
     */
    public function steps()
    {
        return $this->belongsToMany(Step::class)->withPivot('order');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Procedures tienen muchos Rooms. Además contiene un campo llamado 'duration'.
     */
    public function rooms()
    {
        return $this->belongsToMany(Room::class)->withPivot('duration');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Procedure tiene muchos Appointments.
     */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Procedure tiene muchos RequestedProcedures.
     */
    public function requestedProcedures()
    {
        return $this->hasMany(RequestedProcedure::class);
    }

     public function modality() {
        return $this->belongsTo('\App\Modality');
    }

    public function getEquipments() {
        $steps = $this->steps()->get();
        $elements = new Collection();

        foreach($steps as $step) {
            $step->load(['equipment']);
            $elements = $elements->merge( $step->equipment );
        }

        return $elements;
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para llevar todos los valores booleanos a 0.
     */
    public function setZeros()
    {
        $this->active = 0;
        $this->interview = 0;
        $this->mammography = 0;
        $this->worklist = 0;
        $this->images = 0;
        $this->radiologist = 0;
        $this->technician = 0;
        $this->save();
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
