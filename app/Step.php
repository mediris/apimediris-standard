<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
     */
    protected $fillable = [
      'administrative_ID', 'description', 'indications', 'active'
    ];

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Un Step tiene un Equipment.
     */
    public function equipment()
    {
        return $this->belongsToMany(Equipment::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Steps tienen Muchos Consumables.
     */
    public function consumables()
    {
        return $this->belongsToMany(Consumable::class);
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Relación: Muchos Steps tienen Muchos Procedures. Además tiene un campo 'order'.
     */
    public function procedures()
    {
        return $this->belongsToMany(Procedure::class)->withPivot('order');
    }

    /**
     * @fecha: 25-11-2016
     * @programador: Juan Bigorra / Pascual Madrid
     * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
     */
    public function active()
    {
        $this->active = $this->active == 1 ? 0 : 1;
        $this->save();
    }
}
