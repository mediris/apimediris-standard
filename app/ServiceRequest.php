<?php
    
    namespace App;
    
    use Illuminate\Database\Eloquent\Model;
    
    class ServiceRequest extends Model {
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Campos que pueden ser llenados a través de eloquent (los que no salgan aquí no podrán ser llenados).
         */
        protected $fillable = [
            'anonymous',
            'patient_id',
            'request_status_id',
            'patient_type_id',
            'source_id',
            'user_id',
            'referring_id',
            'answer_id',
            'weight',
            'height',
            'pregnancy_status_id',
            //'smoking_status_id',
            'issue_date',
            'patient_state_id',
            'last_menstrual_date',
            'comments',
            'active',
            'patient_last_name',
            'patient_first_name',
            'patient_identification_id',
            'patient_sex_id',
            'patient_cellphone',
            'institution_id',
            'medical_visitor_id'
        ];
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un Source.
         */
        public function source () {
            
            return $this->belongsTo(Source::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un RequestStatus. 1 => created, 2 => scheduled, 3 => admitted, 4 => discharged.
         */
        public function requestStatus () {
            
            return $this->belongsTo(RequestStatus::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un SmokingStatus. 1 => yes, 2 => no, 3 => unknow.
         */
        public function smokingStatus () {
            
            return $this->belongsTo(SmokingStatus::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene muchos RequestDocuments.
         */
        public function requestDocuments () {
            
            return $this->hasMany(RequestDocument::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un Answer.
         */
        public function answer () {
            
            return $this->belongsTo(Answer::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un PregnancyStatus.
         */
        public function pregnancyStatus () {
            
            return $this->belongsTo(PregnancyStatus::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un PatientType.
         */
        public function patientType () {
            
            return $this->belongsTo(PatientType::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un Referring.
         */
        public function referring () {
            
            return $this->belongsTo(Referring::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene muchos RequestedProcedures.
         */
        public function requestedProcedures () {
            
            return $this->hasMany(RequestedProcedure::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Relación: Un ServiceRequest tiene un PatientState.
         */
        public function patientState () {
            
            return $this->belongsTo(PatientState::class);
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Función para crear los RequestDocuments al momento de guardar el ServiceRequest y asociarlos al mismo.
         */
        public function addRequestDocuments ($requestDocuments) {
            
            /*foreach ($this->requestDocuments()->get() as $requestDocument) {
                $requestDocument->delete();
            }*/

            $ret = [];
            
            foreach ($requestDocuments as $requestDocument) {
                $data = [
                    'type'               => $requestDocument['type'],
                    'name'               => $requestDocument['name'],
                    'description'        => $requestDocument['description'],
                    'location'           => $requestDocument['location'],
                    'service_request_id' => $this->id,
                ];
                
                if (isset( $requestDocument['id'] )) {
                    $requestDocument = RequestDocument::find($requestDocument['id']);
                    $requestDocument->update($data);
                }
                else {
                    $requestDocument = new RequestDocument($data);
                    $requestDocument->save();
                }

                $ret[] = $requestDocument;
            }

            return $ret;
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Función para limpiar los RequestDocuments asociados al serviceRequest.
         */
        public function clearAllRequestDocuments () {
            
            if (sizeof($this->requestDocuments()->get()) > 0) {
                foreach ($this->requestDocuments()->get() as $requestDocument) {
                    $requestDocument->delete();
                }
            }
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Función para crear los RequestedProcedures al momento de guardar el ServiceRequest y asociarlos al mismo.
         * @change: 15-06-2017 Se quitó el campo 'bi_rad_id' => 999 de $data
         */
        public function addRequestedProcedures ($requestedProcedures) {
            
            $patientType = $this->patientType()->get()->first();

            foreach ($this->requestedProcedures()->get() as $requestedProcedure) {
                $requestedProcedure->delete();
            }

            foreach ($requestedProcedures as $requestedProcedure) {

                if($patientType->admin_aprob ){
                    
                    $requestedProcedure['requested_procedure_status_id'] = 1;

                        $requestedProcedure['requested_procedure_status_id'] = $requestedProcedure['preadmission'] == 0 ? 2 : 1;

                    if ($requestedProcedure['technician'] == 0 && $requestedProcedure['radiologist'] == 0) {

                         $requestedProcedure['requested_procedure_status_id'] = 6;

                    } elseif ($requestedProcedure['technician'] == 0 && $requestedProcedure['radiologist'] == 1) {

                            $requestedProcedure['requested_procedure_status_id'] = 3;
                    }
                }else{
                    
                    $requestedProcedure['requested_procedure_status_id'] = 2;
                }


                if ($requestedProcedure['blocking_user_id_radiologist'] != null) {

                    $blockStatus = 1;

                    $radiologist = \DB::table('mediris.users')
                    ->where('id', $requestedProcedure['blocking_user_id_radiologist'])
                    ->get();

                    $radiologistName = $radiologist[0]->first_name . " " . $radiologist[0]->last_name;

                }else{

                    $blockStatus = 0;
                    $radiologistName = "";
                }


                $data = [
                    'service_request_id'            => $this->id,
                    'procedure_id'                  => $requestedProcedure['procedure_id'],
                    'comments'                      => $requestedProcedure['comments'],
                    'urgent'                        => isset( $requestedProcedure['urgent'] ) ? $requestedProcedure['urgent'] : 0,
                    'requested_procedure_status_id' => $requestedProcedure['requested_procedure_status_id'],
                    'plates_size_id'                => 1,
                    'category_id'                   => 1,
                    'sub_category_id'               => 1,
                    'blocking_user_id_radiologist' => isset( $requestedProcedure['blocking_user_id_radiologist'] ) ? $requestedProcedure['blocking_user_id_radiologist'] : 0,
                    'blocking_user_name_radiologist' => $radiologistName,
                    'block_radiologist_status' => $blockStatus,
                    'reference_number' => isset( $requestedProcedure['reference_number'] ) ? $requestedProcedure['reference_number'] : 0 
                ];
                
                if ( isset($requestedProcedure['bi_rad_id']) ) {
                    $data['bi_rad_id'] = $requestedProcedure['bi_rad_id'];
                }
                
                if (isset( $requestedProcedure['id'] )) {
                    $data['id'] = $requestedProcedure['id'];
                }
                
                $requestedProcedure = new RequestedProcedure($data);

                $requestedProcedure->save();
            }

            $this->load(['requestedProcedures']);
        }
        
        public function editRequestedProcedures ($requestedProcedures) {
            
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Función para cambiar el campo active de 0 a 1 y viceversa.
         */
        public function active () {
            
            $this->active = $this->active == 1 ? 0 : 1;
            $this->save();
        }
        
        /**
         * @fecha: 25-11-2016
         * @programador: Juan Bigorra / Pascual Madrid
         * @objetivo: Función para obtener los requestedProcedures asociados a ese ServiceRequest
         */
        public function lock () {
            
            $requestedProcedures = $this->requestedProcedures()->get();
            
            return $requestedProcedures;
        }
    }
