@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Roles</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('roles.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            @foreach($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td><a href="{{ route('roles.show', [$role]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('roles.edit', [$role]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('roles.delete', [$role]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection