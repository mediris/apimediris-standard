@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>{{ ucfirst(trans('messages.consumables')) }}</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif


        <a href="{{ route('consumables.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Units</th>
                <th>Actions</th>
            </tr>
            @foreach($consumables as $consumable)
                <tr>
                    <td>{{ $consumable->id }}</td>
                    <td>{{ $consumable->description }}</td>
                    <td>{{ $consumable->units }}</td>
                    <td><a href="{{ route('consumables.show', [$consumable]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('consumables.edit', [$consumable]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('consumables.delete', [$consumable]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection