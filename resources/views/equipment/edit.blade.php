@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit an Equipment</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('equipment.edit', [$equipment]) }}">
                            {!! csrf_field() !!}



                            <div class="form-group{{ $errors->has('ae_title') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="ae_title" value="{{ $equipment->ae_title }}">

                                    @if ($errors->has('ae_title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ae_title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $equipment->name }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Is Active?</label>

                                <div class="col-md-6">
                                    <input type="checkbox" class="form-control" name="active" value="1" {{ $equipment->isActive() }}>

                                    @if ($errors->has('active'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('active') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('room_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ ucfirst(trans('messages.room')) }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="room_id">
                                        <option value="">Select</option>
                                        @foreach($rooms as $room)
                                            @if($room->id == $equipment->room->id)
                                                <option value="{{ $room->id }}" selected>{{ $room->name }}</option>
                                            @else
                                                <option value="{{ $room->id }}">{{ $room->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('room_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('room_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('modality_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ ucfirst(trans('messages.modality')) }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="modality_id">
                                        <option value="">Select</option>
                                        @foreach($modalities as $modality)
                                            @if($modality->id == $equipment->modality_id)
                                                <option value="{{ $modality->id }}" selected>{{ $modality->name }}</option>
                                            @else
                                                <option value="{{ $modality->id }}">{{ $modality->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @if ($errors->has('modality_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('modality_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
