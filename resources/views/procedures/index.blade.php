@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>{{ ucfirst(trans('messages.procedures')) }}</h1>

        @if(Session::has('message'))
            <div class="{{ Session::get('class') }}">
                <p>{{ Session::get('message') }}</p>
            </div>
        @endif

        <a href="{{ route('procedures.add') }}">{{ ucfirst(trans('messages.add')) }} +</a>

        <table class="table table-bordered">
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            @foreach($procedures as $procedure)
                <tr>
                    <td>{{ $procedure->id }}</td>
                    <td>{{ $procedure->description }}</td>
                    <td><a href="{{ route('procedures.show', [$procedure]) }}">{{ ucfirst(trans('messages.show')) }}</a> | <a href="{{ route('procedures.edit', [$procedure]) }}">{{ ucfirst(trans('messages.edit')) }}</a> | <a href="{{ route('procedures.delete', [$procedure]) }}" onclick="return confirm('{{ trans("messages.delete") }}')">{{ ucfirst(trans('messages.purge')) }}</a></td>
                </tr>
            @endforeach
        </table>

    </div>


@endsection