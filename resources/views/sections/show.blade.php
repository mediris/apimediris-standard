@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                    Section
                </div>
                <div class="card-block">
                    <h1 class="card-title">{{ strtoupper($section->name) }}</h1>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn">Edit</a>
                </div>
            </div>

        </div>

    </div>

@endsection