@extends('layouts.app')

@section('title','ADD SECTIONS');

@section('content')
    <div class="container">
        <div class="forms">
            <form method="post" action="{{ route('sections.add') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <fieldset id="fld-section">
                    <div class="row">
                        <div class="col-md-3">
                            <legend>Add a Section</legend>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div>
                                <label for='name'>Section Name</label>
                            </div>
                            <div>
                                <input type="text" name="name" id="name" class="input-field form-control user btn-style"/>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-12">
                            <div>
                                <label for='name'>Parent Section</label>
                            </div>
                            <div class="col-sm-12">
                                <ul>
                                    <li>

                                        <div class="radio">
                                            <label><input type="radio" name="parent_id" value="0">root</label>
                                        </div>


                                        <ul>
                                            @foreach($roots as $root)
                                                <li>

                                                    <div class="radio">
                                                        <label><input type="radio" name="parent_id" value="{{ $root->id }}">{{ $root->name }}</label>
                                                    </div>

                                                    {{ $root->printChilds() }}
                                                </li>
                                            @endforeach

                                        </ul>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-md-12">
                            <div>
                                <button class="btn btn-form ladda-button btn-submit-form" data-style="expand-left" type="submit"><span class="ladda-label">Guardar</span></button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection