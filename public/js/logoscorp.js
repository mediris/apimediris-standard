/**
 * Created by aodreman on 3/24/15.
 */




// Lazy load / La pagina cargo en su totalidad
$(window).bind("load", function() {
    // Scripts a ejecutar

    showBusquedaAvanzada();
    toggleSidebarMenu();
    manageSidebar();
    // translateRangeCalendar();
    slider();


});

// Muestra el div de la busqueda avanzada en la pagina busqueda.html
function showBusquedaAvanzada() {

    $("#b_avanzada_btn").on('click', function() {

        $("#b_avanzada").toggle();
    });
}

// Debido a un problema con la traduccion del plugin datarangepicker se hace la traduccion aqui
function translateRangeCalendar() {

    var control = $(".ranges").find("ul");
    control.find("li").each(function() {

        var current = $(this);
        switch (current.text()) {

            case "Today":
                current.text("Hoy");
                break;

            case "Yesterday":
                current.text("Ayer");
                break;

            case "Last 7 Days":
                current.text("Ultimos 7 dias");
                break;

            case "Last 30 Days":
                current.text("Ultimos 30 dias");
                break;

            case "This Month":
                current.text("Este mes");
                break;

            case "Last Month":
                current.text("Mes pasado");
                break;

            default:
                break;
        }
    });
}

// Rounded-up value for percentage animation
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    return val;
}

function slider() {
    /*$(".fullwidthbanner").css("display","block");
    $('.fullwidthbanner').revolution(
        {
            delay:9000,
            startheight:490,
            startwidth:890,

            hideThumbs:200,

            thumbWidth:100,
            thumbHeight:50,
            thumbAmount:5,

            navigationType:"none",
            navigationArrows:"verticalcentered",
            navigationStyle:"navbar",
            touchenabled:"on",
            onHoverStop:"on",

            navOffsetHorizontal:0,
            navOffsetVertical:20,

            shadow:1,
            fullWidth:"on"

        });*/





}