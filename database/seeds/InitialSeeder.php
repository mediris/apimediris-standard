<?php

use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Se incorpora la columna oldId a la tabla procedures
        DB::insert(DB::raw("ALTER TABLE procedures ADD COLUMN oldId VARCHAR(10) NULL AFTER updated_at"));
        // Se incorpora la columna oldId a la tabla templates
        DB::insert(DB::raw("ALTER TABLE templates ADD COLUMN oldId VARCHAR(10) NULL AFTER updated_at"));
        // Se incorpora la columna oldId a la tabla rooms
        DB::insert(DB::raw("ALTER TABLE rooms ADD COLUMN oldId VARCHAR(10) NULL AFTER updated_at"));
        // Se incorpora la columna oldId a la tabla equipment
        DB::insert(DB::raw("ALTER TABLE equipment ADD COLUMN oldId VARCHAR(10) NULL AFTER updated_at"));
    }
}
