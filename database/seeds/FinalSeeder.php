<?php

use Illuminate\Database\Seeder;

class FinalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // se elimina la colummna oldId de la tabla procedures
        DB::insert(DB::raw("ALTER TABLE procedures DROP COLUMN oldId;"));
        // se elimina la colummna oldId de la tabla templates
        DB::insert(DB::raw("ALTER TABLE templates DROP COLUMN oldId;"));
        // se elimina la colummna oldId de la tabla rooms
        DB::insert(DB::raw("ALTER TABLE rooms DROP COLUMN oldId;"));
        // se elimina la colummna oldId de la tabla equipment
        DB::insert(DB::raw("ALTER TABLE equipment DROP COLUMN oldId;"));
    }
}
