<?php

use Illuminate\Database\Seeder;
use App\SubCategory;

class SubCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subcategories = [
            '-',
            'Normal',
            'Inflamación',
            'Neoplasias',
            'Trauma',
            'Metabólico, Endocrino, Tóxico',
            'Enfer. Sistémicas',
            'Vascular',
            'Misceláneos',
            'Congénito',
        ];

        foreach($subcategories as $subcategory)
        {
            SubCategory::create([
                'active' => 1,
                'description' => $subcategory,
                'language' => 'es'
            ]);
        }
    }
}
