<?php

use Illuminate\Database\Seeder;
use App\RequestedProcedureStatus;

class RequestedProcedureStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['to-admit', 'to-do', 'to-dictate', 'to-transcribe', 'to-approve', 'finished', 'suspended'];
        $status_color = ['yellow','yellow', 'green', 'red'];

        foreach($statuses as $key => $status)
        {
            RequestedProcedureStatus::create([
                'description' => $status
            ]);
        }
    }
}
