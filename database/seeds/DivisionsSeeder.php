<?php

use Illuminate\Database\Seeder;
use App\Division;

class DivisionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Division::create([
            'active' => 1,
            'name' => 'first',
            'active' => 1,
        ]);

        Division::create([
            'active' => 1,
            'name' => 'second',
            'active' => 1,
        ]);

        Division::create([
            'active' => 1,
            'name' => 'third',
            'active' => 1,
        ]);
    }
}
