<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Template;

use Maatwebsite\Excel\Facades\Excel;

class TemplatesSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        // se carga el seed a partir del archivo modalidad.csv
        Excel::load('database/exportFile/plantilla.csv', function ( $reader ){

            foreach( $reader->get() as $seed ){
                // si el registro se encuentra activo
                if ($seed->activo == 1)
                    Template::create([
                        'description' => $seed->descripcion,
                        'active' => $seed->activo,
                        'template' => $seed->texto,
                        'oldId' => $seed->id
                    ]);
            }
        });
    }
}
