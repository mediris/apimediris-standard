<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameUserIdReferrings extends Migration{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('referrings', function ($table) {
           $table->renameColumn('user_id', 'id_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('referrings', function ($table) {
            if (Schema::hasColumn('referrings', 'id_user')) {
                $table->renameColumn('id_user', 'user_id');
            }
        });
    }
}
