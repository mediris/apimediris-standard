<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->string('name', 50)->index();
            $table->string('description', 250);
            $table->string('administrative_ID', 45)->unique()->index();
            $table->integer('division_id')->unsigned()->index();
            $table->integer('quota');
            $table->integer('equipment_number');
            $table->string('start_hour', 10);
            $table->string('end_hour', 10);
            $table->integer('block_size');
            $table->string('avoided_blocks', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
