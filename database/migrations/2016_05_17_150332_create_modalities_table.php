<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modalities', function (Blueprint $table) {

            $table->increments('id');
            $table->boolean('active');
            $table->string('name', 10)->index();
            $table->integer('parent_id')->unsigned()->index();
            $table->integer('level');
            $table->string('description', 250);
            $table->boolean('requires_additional_fields');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modalities');
    }
}
