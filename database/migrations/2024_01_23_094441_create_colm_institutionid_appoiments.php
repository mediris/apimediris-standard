<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmInstitutionidAppoiments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        Schema::table('appointments', function (Blueprint $table) {

            $table->integer('institution_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        Schema::table('appointments', function (Blueprint $table) {

            $table->dropColumn('institution_id');

        });
    }
}
