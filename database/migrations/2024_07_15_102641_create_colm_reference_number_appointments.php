<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmReferenceNumberAppointments extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('appointments', function (Blueprint $table) {
            $table->string('reference_number', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        Schema::table('appointments', function (Blueprint $table) {
        
            if (Schema::hasColumn('appointments', 'reference_number')) {
                
                $table->dropColumn('reference_number');
            }
        
        });
    }
}
