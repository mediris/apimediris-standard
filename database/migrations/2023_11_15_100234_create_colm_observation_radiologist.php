<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColmObservationRadiologist extends Migration{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('requested_procedures', function (Blueprint $table) {
            $table->text('observation_radiologist');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('requested_procedures', function (Blueprint $table) {
            $table->dropColumn('observation_radiologist');
        });
    }
}
